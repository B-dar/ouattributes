# Open Unity Attributes
Open Unity Attributes (OUA) is an extension for the Unity Inspector originaly forked from [Naughty Attributes][LinkNaughtyAttributes].

It expands the range of attributes that Unity provides so that you can create powerful inspectors without the need of custom editors or property drawers. It also provides attributes that can be applied to non-serialized fields or functions.

It is implemented by replacing the default Unity Inspector. This means that if you have any custom editors, **Open Unity Attributes** will not work with them. All of your custom editors and property drawers are not affected in any way.

___
## How to Install

1. First, open the **Package Manager Window**  
![Open Package Manager](https://gitlab.com/wcampospro/ouattributes/raw/61f1ccabebefa2c9ed8550455a42668dcdadf96d/Documentation~/Images/Unity_PackageManagerWindow.png)

2. Second, on the top left **+** sign, select Add Package from git URL...  
![PackageManager Add from Git URL](https://gitlab.com/wcampospro/ouattributes/raw/61f1ccabebefa2c9ed8550455a42668dcdadf96d/Documentation~/Images/PackageManager_GitURL.png)

3. Third, paste The ***HTTPS GitURL*** in the text field

The library is ready to be used...
___
## System Requirements
Unity 2019.3 or later versions.

___
## Dependencies
No Dependencies, however  
While this Library does not depend on other Libraries, it is itself a requirement for other libraries such as  
- [*OUTools*][LinkOUTools]
- [*OUSscriptableLibrary*][LinkOUSscriptableLibrary]

___
## Drawer Attributes
Provide special draw options to serialized fields.
A field can have only one DrawerAttribute. If a field has more than one, only the bottom one will be used.

___
## PENDING Documentation per Attribute
- Help Complete ReadMe Documentation
- You can always find Exmaples within the library

___
## How to create your own attributes
Lets say you want to implement your own **[ReadOnly]** attribute.

First you have to create a **ReadOnlyAttribute** class
``` csharp
[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
public class ReadOnlyAttribute : DrawerAttribute
{
    //If required to have some field per Attribute, make them **readonly** unless necessary
    public readonly string SomeRelevantParameter;
}
```

Then you need to create a drawer for that attribute
``` csharp
[PropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyPropertyDrawer : PropertyDrawer
{
	public override void DrawProperty(SerializedProperty property)
	{
		var readOnlyAttribute = property.GetAttribute<ReadOnlyAttribute>(); //This is how you get a reference to the actual Attribute
		var relevantParameter = readOnlyAttribute.SomeRelevantParameter; //This is how you use the Attributes "parameters"/Fields

		GUI.enabled = false;
		EditorGUILayout.PropertyField(property, true);
		GUI.enabled = true;
	}
}
```

Last, in order for the editor to recognize the drawer for this attribute, you have to press the **Tools/OpenUnityAttribuets/Update Attributes Database** menu item in the editor.

___
## Want to Help? How to contribute to this Repo

For all code, please follow these [Coding Standards and Naming Conventions][LinkUnityCodingStandards].
Simply work on your fork, create a feature branch and when done please submit a pull request.

___
## License
MIT License

Copyright (c) 2019 Willy Campos

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[LinkNaughtyAttributes]: https://github.com/dbrizov/NaughtyAttributes
[LinkOUTools]: https://gitlab.com/wcampospro/outools
[LinkOUSscriptableLibrary]: https://gitlab.com/wcampospro/ousl
[LinkUnityCodingStandards]: https://gitlab.com/wcampospro/CodingConvention_Unity

File Edited to Test Pull Request Notifications.
