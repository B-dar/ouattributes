﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// The base class for all Open Unity attributes
    /// </summary>
    public class OpenUnityAttribute : Attribute { }

}
