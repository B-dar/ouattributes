using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class HideIfAttribute : ShowIfAttribute
    {
        /// <summary>
        /// Hides the Field in the inspector if the return value of the condition evaluates to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example hides the Field if the return value of the field _CanHide is true
        /// </summary>
        /// <code>
        /// {
        /// public class MyClass : Monobehaviour
        ///     [HideIf("_CanHide")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool _CanHide = true;
        /// }
        /// </code>
        /// <param name="condition">name of the ***bool condition*** to evaluate</param>
        public HideIfAttribute(string condition) : base(condition)
        {
            Reversed = true;
        }

        /// <summary>
        /// Hides the Field in the inspector if the return value of the conditions evaluate to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example hides the Field if the return value of at least one of the CanHide# conditionals is true
        /// </summary>
        /// <code>
        /// public class MyClass : Monobehaviour
        /// {
        ///     [HideIf(ConditionOperator.Or, "CanHide1", "CanHide2")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool CanHide1() => true;
        ///     private bool CanHide2() => false;
        /// }
        /// </code>
        /// <param name="conditionOperator">Either a logical AND or a logical OR</param>
        /// <param name="conditions">string array of the names of the **bool conditions** to evaluate</param>
        public HideIfAttribute(ConditionOperator conditionOperator, params string[] conditions) : base(conditionOperator, conditions)
        {
            Reversed = true;
        }
    }
}
