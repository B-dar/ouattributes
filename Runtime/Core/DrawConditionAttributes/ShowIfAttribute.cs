using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ShowIfAttribute : DrawConditionAttribute
    {
        public string[] Conditions { get; private set; }
        public ConditionOperator ConditionOperator { get; private set; }
        public bool Reversed { get; protected set; }

        /// <summary>
        /// Shows the Field in the inspector if the return value of the condition evaluates to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example shows the Field if the return value of the function CanShow() is true
        /// </summary>
        /// <code>
        /// {
        /// public class MyClass : Monobehaviour
        ///     [ShowIf("CanShow")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool CanShow() => true;
        /// }
        /// </code>
        /// <param name="condition">name of the ***bool condition*** to evaluate</param>
        public ShowIfAttribute(string condition)
        {
            ConditionOperator = ConditionOperator.And;
            Conditions = new string[1] { condition };
        }

        /// <summary>
        /// Shows the Field in the inspector if the return value of the conditions evaluate to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example shows the Field if the return value of at least one of the CanShow# conditionals is true
        /// </summary>
        /// <code>
        /// public class MyClass : Monobehaviour
        /// {
        ///     [ShowIf(ConditionOperator.Or, "CanShow1", "CanShow2")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool CanShow1() => true;
        ///     private bool CanShow2() => false;
        /// }
        /// </code>
        /// <param name="conditionOperator">Either a logical **AND** or a logical **OR**</param>
        /// <param name="conditions">string array of the names of the **bool conditions** to evaluate</param>
        public ShowIfAttribute(ConditionOperator conditionOperator, params string[] conditions)
        {
            ConditionOperator = conditionOperator;
            Conditions = conditions;
        }
    }
}
