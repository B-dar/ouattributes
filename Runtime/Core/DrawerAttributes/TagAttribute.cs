using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays a dropdown list with the available Tags
    /// Only works with Strings
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class TagAttribute : DrawerAttribute
    {
    }
}
