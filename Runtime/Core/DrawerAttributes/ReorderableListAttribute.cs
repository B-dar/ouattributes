﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays a list as a reorderable container
    /// Allows to move the order of items in the inspector
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReorderableListAttribute : DrawerAttribute
    {
    }
}
