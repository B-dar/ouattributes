using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Creates a slider with a min and max values
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class MinMaxSliderAttribute : DrawerAttribute
    {
        public readonly float MinValue;
        public readonly float MaxValue;

        /// <summary>
        /// Creates a slider with a min and max values
        /// minValue must be less than maxValue
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        public MinMaxSliderAttribute(float minValue, float maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }
    }
}
