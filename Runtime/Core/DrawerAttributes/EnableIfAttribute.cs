using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class EnableIfAttribute : DrawerAttribute
    {
        public readonly string[] Conditions;
        public readonly ConditionOperator ConditionOperator;
        public bool Reversed { get; protected set; }

        /// <summary>
        /// Enables the Field in the inspector if the return value of the condition evaluates to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example enables the Field if the return value of the function _IsEnabled is true
        /// </summary>
        /// <code>
        /// {
        /// public class MyClass : Monobehaviour
        ///     [EnableIf("_IsEnabled")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool _IsEnabled = true;
        /// }
        /// </code>
        /// <param name="condition">name of the ***bool condition*** to evaluate</param>
        public EnableIfAttribute(string condition)
        {
            ConditionOperator = ConditionOperator.And;
            Conditions = new string[1] { condition };
        }

        /// <summary>
        /// Enables the Field in the inspector if the return value of the conditions evaluate to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example Enables the Field if the return value of at least one of the _IsEnabled# conditionals is true
        /// </summary>
        /// <code>
        /// public class MyClass : Monobehaviour
        /// {
        ///     [EnableIf(ConditionOperator.Or, "_IsEnable1", "_IsEnable2")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool _IsEnable1 = true;
        ///     private bool _IsEnable2 = false;
        /// }
        /// </code>
        /// <param name="conditionOperator">Either a logical **AND** or a logical **OR**</param>
        /// <param name="conditions">string array of the names of the **bool conditions** to evaluate</param>
        public EnableIfAttribute(ConditionOperator conditionOperator, params string[] conditions)
        {
            ConditionOperator = conditionOperator;
            Conditions = conditions;
        }
    }
}
