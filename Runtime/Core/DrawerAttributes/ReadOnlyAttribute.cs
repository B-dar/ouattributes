﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Makes the field a read only field, visible but cannot be modified in the inspector
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ReadOnlyAttribute : DrawerAttribute
    {
    }
}
