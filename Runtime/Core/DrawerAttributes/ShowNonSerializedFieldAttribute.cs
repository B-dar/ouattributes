﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Shows non-serialized fields at the bottom of the inspector before the method buttons.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ShowNonSerializedFieldAttribute : DrawerAttribute
    {
    }
}
