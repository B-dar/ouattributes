using System;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays a progressbar instead of the actual value, cannot modify value through inspector
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ProgressBarAttribute : DrawerAttribute
    {
        public readonly string Name;
        public readonly float MaxValue;
        public readonly Color ActualColor;

        /// <summary>
        /// Displays a progressbar instead of the actual value, cannot modify value through inspector
        /// </summary>
        /// <param name="name">the name that will appear inside the progressbar</param>
        /// <param name="maxValue">the Max Value to be taken to convert into percent</param>
        /// <param name="color">the fill color of the progressbar</param>
        public ProgressBarAttribute(string name = "", float maxValue = 100, ProgressBarColor color = ProgressBarColor.Blue)
        {
            Name = name;
            MaxValue = maxValue;
            ActualColor = GetColor(color);
        }

        private Color GetColor(ProgressBarColor color)
        {
            switch (color)
            {
                case ProgressBarColor.Red: return new Color32(255, 0, 00, 255);         //#FF0000
                case ProgressBarColor.Yellow: return new Color32(255, 211, 0, 255);     //#ffd300
                case ProgressBarColor.Orange: return new Color32(255, 128, 0, 255);     //#ff7d00
                case ProgressBarColor.Cyan: return new Color32(0, 255, 255, 255);       //#00FFFF
                case ProgressBarColor.Blue: return new Color32(0, 135, 189, 255);       //#0087bd
                case ProgressBarColor.FullBlue: return new Color32(0, 0, 255, 255);     //#0000FF
                case ProgressBarColor.Green: return new Color32(102, 255, 0, 255);      //#66ff00
                case ProgressBarColor.Pink: return new Color32(255, 152, 203, 255);     //#ff98cb
                case ProgressBarColor.Fuchsia: return new Color32(255, 152, 203, 255);  //#ff007d
                case ProgressBarColor.Magenta: return new Color32(255, 0, 255, 255);    //#FF00FF
                case ProgressBarColor.Indigo: return new Color32(75, 0, 130, 255);      //#4b0082
                case ProgressBarColor.Violet: return new Color32(127, 0, 255, 255);     //#7f00ff
                case ProgressBarColor.Gray: return new Color32(128, 128, 128, 255);     //#808080
                default: return Color.white; //#FFFFFF
            }
        }
    }

    public enum ProgressBarColor : byte
    {
        Red,
        Yellow,
        Orange,
        Cyan,
        Blue,
        FullBlue,
        Green,
        Pink,
        Fuchsia,
        Magenta,
        Indigo,
        Violet,
        Gray,
        White
    }
}
