using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ButtonAttribute : DrawerAttribute
    {
        public string Text { get; private set; }
        public int SpaceBefore { get; private set; }
        public int SpaceAfter { get; private set; }

        /// <summary>
        /// Draws a Button in the inspector that Calls the Method it is placed on
        /// </summary>
        /// <param name="text">The Text to be displayed on the Button</param>
        /// <param name="spaceBefore">Space in pixels Before the Buttom</param>
        /// <param name="spaceAfter">Space in pixels After the Buttom</param>
        public ButtonAttribute(string text = null, int spaceBefore = 0, int spaceAfter = 0)
        {
            Text = text;
            SpaceBefore = spaceBefore;
            SpaceAfter = spaceAfter;
        }
    }
}
