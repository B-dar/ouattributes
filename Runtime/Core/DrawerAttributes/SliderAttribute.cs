using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Creates a slider in the inspector, must provide min and max values
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class SliderAttribute : DrawerAttribute
    {
        public readonly float MinValue;
        public readonly float MaxValue;

        /// <summary>
        /// Displays a slider that ranges between <paramref name="minValue"/> and <paramref name="maxValue"/>  
        /// </summary>
        public SliderAttribute(float minValue, float maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        /// <summary>
        /// Displays a slider that ranges between <paramref name="minValue"/> and <paramref name="maxValue"/>  
        /// </summary>
        public SliderAttribute(int minValue, int maxValue)
        {
            MaxValue = minValue;
            MaxValue = maxValue;
        }
    }
}
