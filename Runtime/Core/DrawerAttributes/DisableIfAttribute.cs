using System;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class DisableIfAttribute : EnableIfAttribute
    {

        /// <summary>
        /// Disables the Field in the inspector if the return value of the condition evaluates to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example Disables the Field if the return value of the function _IsDisabled is true
        /// </summary>
        /// <code>
        /// {
        /// public class MyClass : Monobehaviour
        ///     [DisableIf("_IsDisabled")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool _IsDisabled = true;
        /// }
        /// </code>
        /// <param name="condition">name of the ***bool condition*** to evaluate</param>
        public DisableIfAttribute(string condition) : base(condition)
        {
            Reversed = true;
        }

        /// <summary>
        /// Disables the Field in the inspector if the return value of the conditions evaluate to true.  
        /// Conditions can be Fields, Properties or function, in general anything that can evaluate to true or false.  
        ///   
        /// The following example Disables the Field if the return value of at least one of the _IsDisabled# conditionals is true
        /// </summary>
        /// <code>
        /// public class MyClass : Monobehaviour
        /// {
        ///     [DisableIf(ConditionOperator.Or, "_IsDisable1", "_IsDisable2")]
        ///     public int _IntValue = 2;
        /// 
        ///     private bool _IsDisable1 = true;
        ///     private bool _IsDisable2 = false;
        /// }
        /// </code>
        /// <param name="conditionOperator">Either a logical **AND** or a logical **OR**</param>
        /// <param name="conditions">string array of the names of the **bool conditions** to evaluate</param>
        public DisableIfAttribute(ConditionOperator conditionOperator, params string[] conditions) : base(conditionOperator, conditions)
        {
            Reversed = true;
        }
    }
}
