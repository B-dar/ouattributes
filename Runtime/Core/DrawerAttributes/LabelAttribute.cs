using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Override default label
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class LabelAttribute : DrawerAttribute
    {
        public readonly string Label;

        public LabelAttribute(string label)
        {
            Label = label;
        }
    }
}
