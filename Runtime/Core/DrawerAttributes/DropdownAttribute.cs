using System;
using System.Collections;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class DropdownAttribute : DrawerAttribute
    {
        public readonly string ValuesFieldName;

        /// <summary>
        /// Creates a Dropdown menu in the inspector with the options found in the <paramref name="valuesFieldName"/> container
        /// </summary>
        /// <param name="valuesFieldName">the name of the container with the desired items, the container must be the same type of the Tagged Field</param>
        public DropdownAttribute(string valuesFieldName)
        {
            ValuesFieldName = valuesFieldName;
        }
    }

    public interface IDropdownList : IEnumerable<KeyValuePair<string, object>>
    {
    }

    public class DropdownList<T> : IDropdownList
    {
        private List<KeyValuePair<string, object>> _Values;

        public DropdownList()
        {
            _Values = new List<KeyValuePair<string, object>>();
        }

        public void Add(string displayName, T value)
        {
            _Values.Add(new KeyValuePair<string, object>(displayName, value));
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static explicit operator DropdownList<object>(DropdownList<T> target)
        {
            DropdownList<object> result = new DropdownList<object>();
            foreach (var kvp in target)
            {
                result.Add(kvp.Key, kvp.Value);
            }

            return result;
        }
    }
}
