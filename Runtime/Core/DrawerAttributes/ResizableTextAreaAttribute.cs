﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Makes a text to have a dynamically sized area in the inspector
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ResizableTextAreaAttribute : DrawerAttribute
    {
    }
}
