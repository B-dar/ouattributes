﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays the native property at the bottom of the inspector.
    /// It supports only certain types (bool, int, long, float, double, string, Vector2, Vector3, Vector4, Color, Bounds, Rect, UnityEngine.Object)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ShowNativePropertyAttribute : DrawerAttribute
    {
    }
}
