using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays a preview thumbnail of the asset that is being referenced
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ShowAssetPreviewAttribute : DrawerAttribute
    {
        public readonly int Width;
        public readonly int Height;

        // <summary>
        /// Displays a preview thumbnail of the asset that is being referenced
        /// </summary>
        /// <param name="width">the width of the preview window</param>
        /// <param name="height">the height of the preview window</param>
        public ShowAssetPreviewAttribute(int width = 64, int height = 64)
        {
            Width = width;
            Height = height;
        }

    }

}
