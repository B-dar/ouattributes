﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Make Scenes appear as Scene dropdown list
    /// Can only be used with Strings to get the string Name and with Ints to get the build Index
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class SceneAttribute : DrawerAttribute
    {
        public readonly bool ShowPath;

        /// <summary>
        /// Make Scenes appear as Scene dropdown list
        /// Can only be used with Strings to get the string Name and with Ints to get the build Index
        /// </summary>
        /// <param name="showPath">Makes it so, so it displays the full path in the inspector</param>
        public SceneAttribute(bool showPath = false)
        {
            ShowPath = showPath;
        }
    }
}
