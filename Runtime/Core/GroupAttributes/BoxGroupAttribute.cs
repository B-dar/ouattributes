﻿using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Groups fields and displays them within a Grouped box
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class BoxGroupAttribute : GroupAttribute
    {
        /// <summary>
        /// Groups fields and displays them within a Grouped box
        /// </summary>
        /// <param name="name">the display name of the Group</param>
        public BoxGroupAttribute(string name = "") : base(name)
        {
        }
    }
}
