namespace GameSavvy.OpenUnityAttributes
{
    public abstract class GroupAttribute : OpenUnityAttribute
    {
        public readonly string Name;

        public GroupAttribute(string name)
        {
            Name = name;
        }
    }
}
