using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Used for limiting the minValue of a field when changed in the inspector
    /// A field can have infinite number of validator attributes
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class MinValueAttribute : ValidatorAttribute
    {
        public readonly float MinValue;

        /// <summary>
        /// Used for limiting the minValue of a field when changed in the inspector
        /// A field can have infinite number of validator attributes
        /// </summary>
        public MinValueAttribute(float minValue)
        {
            MinValue = minValue;
        }

        /// <summary>
        /// Used for limiting the minValue of a field when changed in the inspector
        /// A field can have infinite number of validator attributes
        /// </summary>
        public MinValueAttribute(int minValue)
        {
            MinValue = minValue;
        }
    }
}
