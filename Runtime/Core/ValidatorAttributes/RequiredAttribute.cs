using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays an error message if the reference to this object is null
    /// Can also Log to console
    /// This also works when using the Ref Validation tool from OpenUnityScriptableLibrary
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class RequiredAttribute : ValidatorAttribute
    {
        public readonly string Message;
        public readonly bool LogToConsole;

        /// <summary>
        /// Displays an error message if the reference to this object is null
        /// Can also Log to console
        /// This also works when using the Ref Validation tool from OpenUnityScriptableLibrary
        /// </summary>
        /// <param name="message">A custom message to display</param>
        /// <param name="logToConsole">option to log to console (WARNING: every GUI Update)</param>
        public RequiredAttribute(string message = null, bool logToConsole = false)
        {
            Message = message;
            LogToConsole = logToConsole;
        }
    }
}
