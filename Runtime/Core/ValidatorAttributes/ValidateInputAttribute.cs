using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Validates the input based on the return value of a bool Callback Function
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ValidateInputAttribute : ValidatorAttribute
    {
        public readonly string CallbackName;
        public readonly string Message;

        /// <summary>
        /// Validates the input based on the return value of a "bool Function" callback
        /// </summary>
        /// <param name="callbackName">The **bool Callback Function** name</param>
        /// <param name="message">Custom message to display</param>
        public ValidateInputAttribute(string callbackName, string message = null)
        {
            CallbackName = callbackName;
            Message = message;
        }
    }
}
