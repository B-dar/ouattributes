using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Used for limiting the maxValue of a field when changed in the inspector
    /// A field can have infinite number of validator attributes
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class MaxValueAttribute : ValidatorAttribute
    {
        public readonly float MaxValue;

        /// <summary>
        /// Used for limiting the maxValue of a field when changed in the inspector
        /// A field can have infinite number of validator attributes
        /// </summary>
        public MaxValueAttribute(float maxValue)
        {
            MaxValue = maxValue;
        }

        /// <summary>
        /// Used for limiting the maxValue of a field when changed in the inspector
        /// A field can have infinite number of validator attributes
        /// </summary>
        public MaxValueAttribute(int maxValue)
        {
            MaxValue = maxValue;
        }
    }
}
