namespace GameSavvy.OpenUnityAttributes
{
    public enum ConditionOperator
    {
        And,
        Or
    }
}
