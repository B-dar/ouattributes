using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Displays an info box in the inspector before the normal display of the field
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class InfoBoxAttribute : MetaAttribute
    {
        public readonly string Text;
        public readonly InfoBoxType Type;
        public readonly string VisibleIf;

        /// <summary>
        /// Displays an info box in the inspector before the normal display of the field
        /// </summary>
        /// <param name="text">The info text</param>
        /// <param name="type">The icon to display</param>
        /// <param name="visibleIf">Conditional to display if</param>
        public InfoBoxAttribute(string text, InfoBoxType type = InfoBoxType.Normal, string visibleIf = null)
        {
            Text = text;
            Type = type;
            VisibleIf = visibleIf;
        }

        /// <summary>
        /// Displays an info box in the inspector before the normal display of the field
        /// </summary>
        /// <param name="text">The info text</param>
        /// <param name="visibleIf">Conditional to display if</param>
        public InfoBoxAttribute(string text, string visibleIf) : this(text, InfoBoxType.Normal, visibleIf)
        {
        }
    }

    public enum InfoBoxType
    {
        Normal,
        Warning,
        Error
    }
}
