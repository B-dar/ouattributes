namespace GameSavvy.OpenUnityAttributes
{
    public abstract class MetaAttribute : OpenUnityAttribute
    {
        public int Order;
    }
}
