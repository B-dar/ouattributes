using System;

namespace GameSavvy.OpenUnityAttributes
{
    /// <summary>
    /// Detects a value change made theough the inspector and executes a callback
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class OnValueChangedAttribute : MetaAttribute
    {
        public readonly string CallbackName;

        /// <summary>
        /// Detects a value change made theough the inspector and executes a callback
        /// </summary>
        /// <param name="callbackName">The name of the callback function</param>
        public OnValueChangedAttribute(string callbackName)
        {
            CallbackName = callbackName;
        }
    }
}
