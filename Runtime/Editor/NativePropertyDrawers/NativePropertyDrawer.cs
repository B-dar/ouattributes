﻿using System.Reflection;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class NativePropertyDrawer
    {
        public abstract void DrawNativeProperty(UnityEngine.Object target, PropertyInfo property);
    }
}
