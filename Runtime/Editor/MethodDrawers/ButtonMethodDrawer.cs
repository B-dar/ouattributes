using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [MethodDrawer(typeof(ButtonAttribute))]
    public class ButtonMethodDrawer : MethodDrawer
    {
        public override void DrawMethod(UnityEngine.Object target, MethodInfo methodInfo)
        {
            if (methodInfo.GetParameters().Length == 0)
            {
                ButtonAttribute buttonAttribute = (ButtonAttribute)methodInfo.GetCustomAttributes(typeof(ButtonAttribute), true)[0];
                string buttonText = string.IsNullOrEmpty(buttonAttribute.Text) ? methodInfo.Name : buttonAttribute.Text;
                int spaceBefore = buttonAttribute.SpaceBefore;
                int spaceAfter = buttonAttribute.SpaceAfter;

                if (spaceBefore > 0)
                {
                    GUILayout.Space(spaceBefore);
                }
                if (GUILayout.Button(buttonText))
                {
                    methodInfo.Invoke(target, null);
                }
                if (spaceAfter > 0)
                {
                    GUILayout.Space(spaceAfter);
                }
            }
            else
            {
                string warning = typeof(ButtonAttribute).Name + " works only on methods with no parameters";
                EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, context: target);
            }
        }
    }
}
