﻿using System.Reflection;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class MethodDrawer
    {
        public abstract void DrawMethod(UnityEngine.Object target, MethodInfo methodInfo);
    }
}
