﻿using System;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public class MethodDrawerAttribute : BaseAttribute
    {
        public MethodDrawerAttribute(Type targetAttributeType) : base(targetAttributeType)
        {
        }
    }
}
