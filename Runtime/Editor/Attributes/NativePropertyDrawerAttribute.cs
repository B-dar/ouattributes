﻿using System;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public class NativePropertyDrawerAttribute : BaseAttribute
    {
        public NativePropertyDrawerAttribute(Type targetAttributeType) : base(targetAttributeType)
        {
        }
    }
}
