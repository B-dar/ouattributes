﻿using System;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public interface IAttribute
    {
        Type TargetAttributeType { get; }
    }
}
