﻿using System;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public abstract class BaseAttribute : Attribute, IAttribute
    {
        private Type _TargetAttributeType;

        public BaseAttribute(Type targetAttributeType)
        {
            _TargetAttributeType = targetAttributeType;
        }

        public Type TargetAttributeType
        {
            get
            {
                return _TargetAttributeType;
            }
        }
    }
}
