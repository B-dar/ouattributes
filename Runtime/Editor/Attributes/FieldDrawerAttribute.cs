﻿using System;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public class FieldDrawerAttribute : BaseAttribute
    {
        public FieldDrawerAttribute(Type targetAttributeType) : base(targetAttributeType)
        {
        }
    }
}
