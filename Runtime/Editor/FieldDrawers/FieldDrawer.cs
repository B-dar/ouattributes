﻿using System.Reflection;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class FieldDrawer
    {
        public abstract void DrawField(UnityEngine.Object target, FieldInfo field);
    }
}
