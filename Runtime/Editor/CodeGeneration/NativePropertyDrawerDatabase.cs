// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class NativePropertyDrawerDatabase
    {
        private static Dictionary<Type, NativePropertyDrawer> _DrawersByAttributeType;

        static NativePropertyDrawerDatabase()
        {
            _DrawersByAttributeType = new Dictionary<Type, NativePropertyDrawer>();
            _DrawersByAttributeType[typeof(ShowNativePropertyAttribute)] = new ShowNativePropertyNativePropertyDrawer();

        }

        public static NativePropertyDrawer GetDrawerForAttribute(Type attributeType)
        {
            NativePropertyDrawer drawer;
            if (_DrawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }
    }
}

