// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class PropertyValidatorDatabase
    {
        private static Dictionary<Type, PropertyValidator> _ValidatorsByAttributeType;

        static PropertyValidatorDatabase()
        {
            _ValidatorsByAttributeType = new Dictionary<Type, PropertyValidator>();
            _ValidatorsByAttributeType[typeof(MaxValueAttribute)] = new MaxValuePropertyValidator();
_ValidatorsByAttributeType[typeof(MinValueAttribute)] = new MinValuePropertyValidator();
_ValidatorsByAttributeType[typeof(RequiredAttribute)] = new RequiredPropertyValidator();
_ValidatorsByAttributeType[typeof(ValidateInputAttribute)] = new ValidateInputPropertyValidator();

        }

        public static PropertyValidator GetValidatorForAttribute(Type attributeType)
        {
            PropertyValidator validator;
            if (_ValidatorsByAttributeType.TryGetValue(attributeType, out validator))
            {
                return validator;
            }
            else
            {
                return null;
            }
        }
    }
}

