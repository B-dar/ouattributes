// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class PropertyDrawerDatabase
    {
        private static Dictionary<Type, OUAPropertyDrawer> _DrawersByAttributeType;

        static PropertyDrawerDatabase()
        {
            _DrawersByAttributeType = new Dictionary<Type, OUAPropertyDrawer>();
            _DrawersByAttributeType[typeof(DisableIfAttribute)] = new DisableIfPropertyDrawer();
_DrawersByAttributeType[typeof(DropdownAttribute)] = new DropdownPropertyDrawer();
_DrawersByAttributeType[typeof(EnableIfAttribute)] = new EnableIfPropertyDrawer();
_DrawersByAttributeType[typeof(GetSetAttribute)] = new GetSetPropertyDrawer();
_DrawersByAttributeType[typeof(InlineButtonAttribute)] = new InlineButtonPropertyDrawer();
_DrawersByAttributeType[typeof(LabelAttribute)] = new LabelPropertyDrawer();
_DrawersByAttributeType[typeof(MinMaxSliderAttribute)] = new MinMaxSliderPropertyDrawer();
_DrawersByAttributeType[typeof(OnlyAssetsAttribute)] = new OnlyAssetsPropertyDrawer();
_DrawersByAttributeType[typeof(OnlySceneObjectsAttribute)] = new OnlySceneObjectsPropertyDrawer();
_DrawersByAttributeType[typeof(ProgressBarAttribute)] = new ProgressBarPropertyDrawer();
_DrawersByAttributeType[typeof(ReadOnlyAttribute)] = new ReadOnlyPropertyDrawer();
_DrawersByAttributeType[typeof(ReorderableListAttribute)] = new ReorderableListPropertyDrawer();
_DrawersByAttributeType[typeof(ResizableTextAreaAttribute)] = new ResizableTextAreaPropertyDrawer();
_DrawersByAttributeType[typeof(SceneAttribute)] = new ScenePropertyDrawer();
_DrawersByAttributeType[typeof(ShowAssetPreviewAttribute)] = new ShowAssetPreviewPropertyDrawer();
_DrawersByAttributeType[typeof(SliderAttribute)] = new SliderPropertyDrawer();
_DrawersByAttributeType[typeof(TagAttribute)] = new TagPropertyDrawer();

        }

        public static OUAPropertyDrawer GetDrawerForAttribute(Type attributeType)
        {
            OUAPropertyDrawer drawer;
            if (_DrawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }

        public static void ClearCache()
        {
            foreach (var kvp in _DrawersByAttributeType)
            {
                kvp.Value.ClearCache();
            }
        }
    }
}

