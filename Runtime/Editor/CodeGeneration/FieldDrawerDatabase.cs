// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class FieldDrawerDatabase
    {
        private static Dictionary<Type, FieldDrawer> _DrawersByAttributeType;

        static FieldDrawerDatabase()
        {
            _DrawersByAttributeType = new Dictionary<Type, FieldDrawer>();
            _DrawersByAttributeType[typeof(ShowNonSerializedFieldAttribute)] = new ShowNonSerializedFieldFieldDrawer();

        }

        public static FieldDrawer GetDrawerForAttribute(Type attributeType)
        {
            FieldDrawer drawer;
            if (_DrawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }
    }
}

