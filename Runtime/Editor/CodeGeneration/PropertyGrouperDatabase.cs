// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class PropertyGrouperDatabase
    {
        private static Dictionary<Type, PropertyGrouper> _GroupersByAttributeType;

        static PropertyGrouperDatabase()
        {
            _GroupersByAttributeType = new Dictionary<Type, PropertyGrouper>();
            _GroupersByAttributeType[typeof(BoxGroupAttribute)] = new BoxGroupPropertyGrouper();

        }

        public static PropertyGrouper GetGrouperForAttribute(Type attributeType)
        {
            PropertyGrouper grouper;
            if (_GroupersByAttributeType.TryGetValue(attributeType, out grouper))
            {
                return grouper;
            }
            else
            {
                return null;
            }
        }
    }
}

