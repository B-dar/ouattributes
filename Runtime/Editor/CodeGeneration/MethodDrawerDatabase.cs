// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class MethodDrawerDatabase
    {
        private static Dictionary<Type, MethodDrawer> _DrawersByAttributeType;

        static MethodDrawerDatabase()
        {
            _DrawersByAttributeType = new Dictionary<Type, MethodDrawer>();
            _DrawersByAttributeType[typeof(ButtonAttribute)] = new ButtonMethodDrawer();

        }

        public static MethodDrawer GetDrawerForAttribute(Type attributeType)
        {
            MethodDrawer drawer;
            if (_DrawersByAttributeType.TryGetValue(attributeType, out drawer))
            {
                return drawer;
            }
            else
            {
                return null;
            }
        }
    }
}

