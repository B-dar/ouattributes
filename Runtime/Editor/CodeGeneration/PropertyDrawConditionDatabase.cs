// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class PropertyDrawConditionDatabase
    {
        private static Dictionary<Type, PropertyDrawCondition> _DrawConditionsByAttributeType;

        static PropertyDrawConditionDatabase()
        {
            _DrawConditionsByAttributeType = new Dictionary<Type, PropertyDrawCondition>();
            _DrawConditionsByAttributeType[typeof(HideIfAttribute)] = new HideIfPropertyDrawCondition();
_DrawConditionsByAttributeType[typeof(ShowIfAttribute)] = new ShowIfPropertyDrawCondition();

        }

        public static PropertyDrawCondition GetDrawConditionForAttribute(Type attributeType)
        {
            PropertyDrawCondition drawCondition;
            if (_DrawConditionsByAttributeType.TryGetValue(attributeType, out drawCondition))
            {
                return drawCondition;
            }
            else
            {
                return null;
            }
        }
    }
}

