// This class is auto generated

using System;
using System.Collections.Generic;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class PropertyMetaDatabase
    {
        private static Dictionary<Type, PropertyMeta> _MetasByAttributeType;

        static PropertyMetaDatabase()
        {
            _MetasByAttributeType = new Dictionary<Type, PropertyMeta>();
            _MetasByAttributeType[typeof(InfoBoxAttribute)] = new InfoBoxPropertyMeta();
_MetasByAttributeType[typeof(OnValueChangedAttribute)] = new OnValueChangedPropertyMeta();

        }

        public static PropertyMeta GetMetaForAttribute(Type attributeType)
        {
            PropertyMeta meta;
            if (_MetasByAttributeType.TryGetValue(attributeType, out meta))
            {
                return meta;
            }
            else
            {
                return null;
            }
        }
    }
}

