using UnityEngine;
using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [PropertyDrawer(typeof(LabelAttribute))]
    public class LabelPropertyDrawer : OUAPropertyDrawer
    {
        public override void DrawProperty(SerializedProperty property)
        {
            var labelAttribute = property.GetAttribute<LabelAttribute>();
            var guiContent = new GUIContent(labelAttribute.Label);
            EditorGUILayout.PropertyField(property, guiContent, true);
        }
    }
}
