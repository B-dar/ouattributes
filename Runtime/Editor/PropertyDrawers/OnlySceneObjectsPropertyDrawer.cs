﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [PropertyDrawer(typeof(OnlySceneObjectsAttribute))]
    public class OnlySceneObjectsPropertyDrawer : OUAPropertyDrawer
    {
        public override void DrawProperty(SerializedProperty property)
        {
            var onlySceneObjectsAttribute = property.GetAttribute<OnlySceneObjectsAttribute>();
            bool logToConsole = onlySceneObjectsAttribute.LogToConsole;

            var targetObject = property.serializedObject.targetObject;
            var targetObjectClassType = targetObject.GetType();
            var field = targetObjectClassType.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            string error = property.displayName + " Must Be a Scene Object";

            if (field != null)
            {
                var value = field.GetValue(targetObject);
                if (value != null && value.ToString() != "null" && AssetDatabase.Contains(value as Object))
                {
                    EditorDrawUtility.DrawHelpBox(error, MessageType.Error, context: property.GetTargetObject(), logToConsole: logToConsole);
                }
            }

            field.SetValue
            (
                targetObject,
                EditorGUILayout.ObjectField(property.displayName, field.GetValue(targetObject) as Object, field.FieldType, true)
            );
        }
    }
}
