﻿using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class OUAPropertyDrawer
    {
        public abstract void DrawProperty(SerializedProperty property);

        public virtual void ClearCache()
        {

        }
    }
}
