using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GameSavvy.OpenUnityAttributes.Editor
{

    [PropertyDrawer(typeof(SceneAttribute))]
    public class ScenePropertyDrawer : OUAPropertyDrawer
    {
        public override void DrawProperty(SerializedProperty property)
        {
            if (property.propertyType == SerializedPropertyType.String)
            {
                GetStringScenes(property);
            }
            else if (property.propertyType == SerializedPropertyType.Integer)
            {
                GetIntScenes(property);
            }
            else
            {
                EditorGUILayout.HelpBox($"{property.type} is not supported by SceneAttribute\nUse [string] or [int] instead", MessageType.Warning);
            }
        }

        private void GetStringScenes(SerializedProperty property)
        {
            var sceneAttribute = property.GetAttribute<SceneAttribute>();
            List<string> allScenes = PopulateScenes(sceneAttribute.ShowPath);

            string propertyString = property.stringValue;

            int index = 0;
            for (int i = 0; i < allScenes.Count; i++)
            {
                if (allScenes[i].Contains(propertyString))
                {
                    index = i;
                    break;
                }
            }

            index = EditorGUILayout.Popup(property.displayName, index, allScenes.ToArray());

            if (index < 0 || index >= allScenes.Count)
            {
                index = 0;
            }

            property.stringValue = allScenes[index];
        }

        private void GetIntScenes(SerializedProperty property)
        {
            var sceneAttribute = property.GetAttribute<SceneAttribute>();
            List<string> allScenes = PopulateScenes(sceneAttribute.ShowPath);

            int propertyInt = property.intValue;

            if (propertyInt < 0 || propertyInt >= allScenes.Count)
            {
                propertyInt = 0;
            }

            propertyInt = EditorGUILayout.Popup(property.displayName, propertyInt, allScenes.ToArray());

            if (propertyInt < 0 || propertyInt >= allScenes.Count)
            {
                propertyInt = 0;
            }

            property.intValue = propertyInt;
        }

        private List<string> PopulateScenes(bool showPath)
        {
            List<string> allScenes = new List<string>();
            foreach (var scene in EditorBuildSettings.scenes)
            {
                var fileName = scene.path;
                if (showPath == false)
                {
                    fileName = Path.GetFileName(scene.path);
                    fileName = fileName.Substring(0, fileName.IndexOf("."));
                }
                allScenes.Add(fileName);
            }

            return allScenes;
        }

    }
}
