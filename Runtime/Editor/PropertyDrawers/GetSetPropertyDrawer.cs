using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [PropertyDrawer(typeof(GetSetAttribute))]
    public class GetSetPropertyDrawer : OUAPropertyDrawer
    {
        public override void DrawProperty(SerializedProperty property)
        {
            var getSetAttribute = property.GetAttribute<GetSetAttribute>();

            EditorGUI.BeginChangeCheck();
            var guiContent = new GUIContent(property.displayName);
            EditorGUILayout.PropertyField(property, guiContent, true);

            if (EditorGUI.EndChangeCheck())
            {
                var parent = property.serializedObject.targetObject.GetParentObject(property.propertyPath);
                var type = parent.GetType();
                var fieldInfo = type.GetField(property.propertyPath, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                var propertyInfo = type.GetProperty(getSetAttribute.PropertyName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                if (propertyInfo == null)
                {
                    Debug.LogError("Invalid property name \"" + getSetAttribute.PropertyName + "\"");
                }
                else
                {
                    propertyInfo.SetValue(parent, fieldInfo.GetValue(parent), null);
                }
            }
        }

        
    }
}
