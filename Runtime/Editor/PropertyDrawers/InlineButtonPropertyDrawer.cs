﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [PropertyDrawer(typeof(InlineButtonAttribute))]
    public class InlineButtonPropertyDrawer : OUAPropertyDrawer
    {
        public override void DrawProperty(SerializedProperty property)
        {
            var att = property.GetAttribute<InlineButtonAttribute>();
            var parent = property.serializedObject.targetObject.GetParentObject(property.propertyPath);
            var type = parent.GetType();
            var methodInfo = type.GetMethod(att.MethodName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var methodName = att.Label == null ? property.displayName : att.Label;

            EditorGUI.BeginChangeCheck();
            var guiContent = new GUIContent(property.displayName);
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.PropertyField(property, guiContent, true);
                if (GUILayout.Button(methodName, GUILayout.ExpandWidth(att.ExpandButton)))
                {
                    methodInfo.Invoke(parent, null);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

    }

}
