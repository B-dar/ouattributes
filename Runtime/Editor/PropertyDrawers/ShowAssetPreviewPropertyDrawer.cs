using UnityEngine;
using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [PropertyDrawer(typeof(ShowAssetPreviewAttribute))]
    public class ShowAssetPreviewPropertyDrawer : OUAPropertyDrawer
    {
        public override void DrawProperty(SerializedProperty property)
        {
            EditorDrawUtility.DrawPropertyField(property);

            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                if (property.objectReferenceValue != null)
                {
                    Texture2D previewTexture = AssetPreview.GetAssetPreview(property.objectReferenceValue);
                    if (previewTexture != null)
                    {
                        ShowAssetPreviewAttribute showAssetPreviewAttribute = property.GetAttribute<ShowAssetPreviewAttribute>();
                        int width = Mathf.Clamp(showAssetPreviewAttribute.Width, 0, previewTexture.width);
                        int height = Mathf.Clamp(showAssetPreviewAttribute.Height, 0, previewTexture.height);

                        GUILayout.Label(previewTexture, GUILayout.MaxWidth(width), GUILayout.MaxHeight(height));
                    }
                    else
                    {
                        PrintWarning(property);
                    }
                }
            }
            else
            {
                PrintWarning(property);
            }
        }

        private void PrintWarning(SerializedProperty prop)
        {
            string warning = $"{prop.name} doesn't have an asset preview";
            EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, context: prop.GetTargetObject());
        }
    }
}
