using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [PropertyValidator(typeof(RequiredAttribute))]
    public class RequiredPropertyValidator : PropertyValidator
    {
        public override void ValidateProperty(SerializedProperty property)
        {
            RequiredAttribute requiredAttribute = property.GetAttribute<RequiredAttribute>();
            bool logToConsole = requiredAttribute.LogToConsole;
            if (property.propertyType == SerializedPropertyType.ObjectReference)
            {
                if (property.objectReferenceValue == null)
                {
                    string errorMessage = property.name + " is required";
                    if (string.IsNullOrEmpty(requiredAttribute.Message) == false)
                    {
                        errorMessage = requiredAttribute.Message;
                    }

                    EditorDrawUtility.DrawHelpBox(errorMessage, MessageType.Error, context: property.GetTargetObject(), logToConsole);
                }
            }
            else
            {
                string warning = requiredAttribute.GetType().Name + " works only on reference types";
                EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, context: property.GetTargetObject(), logToConsole);
            }
        }
    }
}
