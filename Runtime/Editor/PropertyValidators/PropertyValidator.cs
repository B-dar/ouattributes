﻿using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class PropertyValidator
    {
        public abstract void ValidateProperty(SerializedProperty property);
    }
}
