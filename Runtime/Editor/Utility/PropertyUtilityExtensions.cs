using UnityEditor;
using System;
using System.Reflection;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public static class PropertyUtilityExtensions
    {
        public static T GetAttribute<T>(this SerializedProperty property) where T : Attribute
        {
            T[] attributes = GetAttributes<T>(property);
            return attributes.Length > 0 ? attributes[0] : null;
        }

        public static T[] GetAttributes<T>(this SerializedProperty property) where T : Attribute
        {
            FieldInfo fieldInfo = GetTargetObject(property).GetField(property.name);

            return (T[])fieldInfo.GetCustomAttributes(typeof(T), true);
        }

        public static UnityEngine.Object GetTargetObject(this SerializedProperty property)
        {
            return property.serializedObject.targetObject;
        }

        public static object GetParentObject(this object obj, string path)
        {
            var fields = path.Split('.');

            if (fields.Length == 1)
            {
                return obj;
            }

            FieldInfo info = obj.GetType().GetField(fields[0], BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            obj = info.GetValue(obj);

            return obj.GetParentObject(string.Join(".", fields, 1, fields.Length - 1));
        }
    }
}
