﻿using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class PropertyMeta
    {
        public abstract void ApplyPropertyMeta(SerializedProperty property, MetaAttribute metaAttribute);
    }
}
