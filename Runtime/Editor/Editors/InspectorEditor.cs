using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(UnityEngine.Object), true)]
    public class InspectorEditor : UnityEditor.Editor
    {
        private SerializedProperty _Script;
        private IEnumerable<FieldInfo> _Fields;
        private IEnumerable<FieldInfo> _NonSerializedFields;
        private IEnumerable<PropertyInfo> _NativeProperties;
        private IEnumerable<MethodInfo> _Methods;
        private HashSet<FieldInfo> _GroupedFields;
        private Dictionary<string, List<FieldInfo>> _GroupedFieldsByGroupName;
        private Dictionary<string, SerializedProperty> _SerializedPropertiesByFieldName;
        private bool _UseDefaultInspector;

        private void OnEnable()
        {
            _Script = serializedObject.FindProperty("m_Script");
            _Fields = target.GetAllFields(f => serializedObject.FindProperty(f.Name) != null);

            // If there are no Open Unity Attributes use default inspector
            if (_Fields.All(f => f.GetCustomAttributes(typeof(OpenUnityAttribute), true).Length == 0))
            {
                _UseDefaultInspector = true;
            }
            else
            {
                _UseDefaultInspector = false;

                _GroupedFields = new HashSet<FieldInfo>(_Fields.Where(f => f.GetCustomAttributes(typeof(GroupAttribute), true).Length > 0));
                _GroupedFieldsByGroupName = new Dictionary<string, List<FieldInfo>>();
                foreach (var groupedField in _GroupedFields)
                {
                    string groupName = (groupedField.GetCustomAttributes(typeof(GroupAttribute), true)[0] as GroupAttribute).Name;

                    if (_GroupedFieldsByGroupName.ContainsKey(groupName))
                    {
                        _GroupedFieldsByGroupName[groupName].Add(groupedField);
                    }
                    else
                    {
                        _GroupedFieldsByGroupName[groupName] = new List<FieldInfo>()
                        {
                            groupedField
                        };
                    }
                }

                _SerializedPropertiesByFieldName = new Dictionary<string, SerializedProperty>();
                foreach (var field in _Fields)
                {
                    _SerializedPropertiesByFieldName[field.Name] = serializedObject.FindProperty(field.Name);
                }
            }

            _NonSerializedFields = target.GetAllFields(
                f => f.GetCustomAttributes(typeof(DrawerAttribute), true).Length > 0 && serializedObject.FindProperty(f.Name) == null);

            _NativeProperties = target.GetAllProperties(
                p => p.GetCustomAttributes(typeof(DrawerAttribute), true).Length > 0);

            _Methods = target.GetAllMethods(
                m => m.GetCustomAttributes(typeof(DrawerAttribute), true).Length > 0);
        }

        private void OnDisable()
        {
            PropertyDrawerDatabase.ClearCache();
        }

        public override void OnInspectorGUI()
        {
            if (_UseDefaultInspector)
            {
                DrawDefaultInspector();
            }
            else
            {
                DrawCustomInspector();
            }

            DrawNonSerializedFields();
            DrawNativeProperties();
            DrawMethods();
        }

        private void DrawMethods()
        {
            foreach (var method in _Methods)
            {
                DrawerAttribute drawerAttribute = (DrawerAttribute)method.GetCustomAttributes(typeof(DrawerAttribute), true)[0];
                MethodDrawer methodDrawer = MethodDrawerDatabase.GetDrawerForAttribute(drawerAttribute.GetType());
                if (methodDrawer != null)
                {
                    methodDrawer.DrawMethod(target, method);
                }
            }
        }

        private void DrawNativeProperties()
        {
            foreach (var property in _NativeProperties)
            {
                DrawerAttribute drawerAttribute = (DrawerAttribute)property.GetCustomAttributes(typeof(DrawerAttribute), true)[0];
                NativePropertyDrawer drawer = NativePropertyDrawerDatabase.GetDrawerForAttribute(drawerAttribute.GetType());
                if (drawer != null)
                {
                    drawer.DrawNativeProperty(target, property);
                }
            }
        }

        private void DrawNonSerializedFields()
        {
            foreach (var field in _NonSerializedFields)
            {
                DrawerAttribute drawerAttribute = (DrawerAttribute)field.GetCustomAttributes(typeof(DrawerAttribute), true)[0];
                FieldDrawer drawer = FieldDrawerDatabase.GetDrawerForAttribute(drawerAttribute.GetType());
                if (drawer != null)
                {
                    drawer.DrawField(target, field);
                }
            }
        }

        private void DrawCustomInspector()
        {
            serializedObject.Update();

            DrawScriptReference();
            DrawFields();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawScriptReference()
        {
            if (_Script != null)
            {
                GUI.enabled = false;
                EditorGUILayout.PropertyField(_Script);
                GUI.enabled = true;
            }
        }

        private void DrawFields()
        {
            HashSet<string> drawnGroups = new HashSet<string>();
            foreach (var field in _Fields)
            {
                if (_GroupedFields.Contains(field))
                {
                    DrawGroupedFields(drawnGroups, field);
                }
                else // Draw non-grouped field
                {
                    ValidateAndDrawField(field);
                }
            }
        }

        private void DrawGroupedFields(HashSet<string> drawnGroups, FieldInfo field)
        {
            string groupName = (field.GetCustomAttributes(typeof(GroupAttribute), true)[0] as GroupAttribute).Name;
            if (drawnGroups.Contains(groupName) == false)
            {
                drawnGroups.Add(groupName);

                PropertyGrouper grouper = GetPropertyGrouperForField(field);
                if (grouper != null)
                {
                    grouper.BeginGroup(groupName);

                    ValidateAndDrawFields(_GroupedFieldsByGroupName[groupName]);

                    grouper.EndGroup();
                }
                else
                {
                    ValidateAndDrawFields(_GroupedFieldsByGroupName[groupName]);
                }
            }
        }

        private void ValidateAndDrawFields(IEnumerable<FieldInfo> fields)
        {
            foreach (var field in fields)
            {
                ValidateAndDrawField(field);
            }
        }

        private void ValidateAndDrawField(FieldInfo field)
        {
            if (ShouldDrawField(field) == false)
            {
                return;
            }

            ValidateField(field);
            ApplyFieldMeta(field);
            DrawField(field);
        }

        private void ValidateField(FieldInfo field)
        {
            ValidatorAttribute[] validatorAttributes = (ValidatorAttribute[])field.GetCustomAttributes(typeof(ValidatorAttribute), true);

            foreach (var attribute in validatorAttributes)
            {
                PropertyValidator validator = PropertyValidatorDatabase.GetValidatorForAttribute(attribute.GetType());
                if (validator != null)
                {
                    validator.ValidateProperty(_SerializedPropertiesByFieldName[field.Name]);
                }
            }
        }

        private bool ShouldDrawField(FieldInfo field)
        {
            // Check if the field has draw conditions
            PropertyDrawCondition drawCondition = GetPropertyDrawConditionForField(field);
            if (drawCondition != null)
            {
                bool canDrawProperty = drawCondition.CanDrawProperty(_SerializedPropertiesByFieldName[field.Name]);
                if (canDrawProperty == false)
                {
                    return false;
                }
            }

            // Check if the field has HideInInspectorAttribute
            HideInInspector[] hideInInspectorAttributes = (HideInInspector[])field.GetCustomAttributes(typeof(HideInInspector), true);
            if (hideInInspectorAttributes.Length > 0)
            {
                return false;
            }

            return true;
        }

        private void DrawField(FieldInfo field)
        {
            EditorGUI.BeginChangeCheck();
            OUAPropertyDrawer drawer = GetPropertyDrawerForField(field);
            if (drawer != null)
            {
                drawer.DrawProperty(_SerializedPropertiesByFieldName[field.Name]);
            }
            else
            {
                EditorDrawUtility.DrawPropertyField(_SerializedPropertiesByFieldName[field.Name]);
            }

            if (EditorGUI.EndChangeCheck())
            {
                OnValueChangedAttribute[] onValueChangedAttributes = (OnValueChangedAttribute[])field.GetCustomAttributes(typeof(OnValueChangedAttribute), true);
                foreach (var onValueChangedAttribute in onValueChangedAttributes)
                {
                    PropertyMeta meta = PropertyMetaDatabase.GetMetaForAttribute(onValueChangedAttribute.GetType());
                    if (meta != null)
                    {
                        meta.ApplyPropertyMeta(_SerializedPropertiesByFieldName[field.Name], onValueChangedAttribute);
                    }
                }
            }
        }

        private void ApplyFieldMeta(FieldInfo field)
        {
            // Apply custom meta attributes
            MetaAttribute[] metaAttributes = field
                .GetCustomAttributes(typeof(MetaAttribute), true)
                .Where(attr => attr.GetType() != typeof(OnValueChangedAttribute))
                .Select(obj => obj as MetaAttribute)
                .ToArray();

            Array.Sort(metaAttributes, (x, y) =>
            {
                return x.Order - y.Order;
            });

            foreach (var metaAttribute in metaAttributes)
            {
                PropertyMeta meta = PropertyMetaDatabase.GetMetaForAttribute(metaAttribute.GetType());
                if (meta != null)
                {
                    meta.ApplyPropertyMeta(_SerializedPropertiesByFieldName[field.Name], metaAttribute);
                }
            }
        }

        private OUAPropertyDrawer GetPropertyDrawerForField(FieldInfo field)
        {
            DrawerAttribute[] drawerAttributes = (DrawerAttribute[])field.GetCustomAttributes(typeof(DrawerAttribute), true);
            if (drawerAttributes.Length > 0)
            {
                OUAPropertyDrawer drawer = PropertyDrawerDatabase.GetDrawerForAttribute(drawerAttributes[0].GetType());
                return drawer;
            }
            else
            {
                return null;
            }
        }

        private PropertyGrouper GetPropertyGrouperForField(FieldInfo field)
        {
            GroupAttribute[] groupAttributes = (GroupAttribute[])field.GetCustomAttributes(typeof(GroupAttribute), true);
            if (groupAttributes.Length > 0)
            {
                PropertyGrouper grouper = PropertyGrouperDatabase.GetGrouperForAttribute(groupAttributes[0].GetType());
                return grouper;
            }
            else
            {
                return null;
            }
        }

        private PropertyDrawCondition GetPropertyDrawConditionForField(FieldInfo field)
        {
            DrawConditionAttribute[] drawConditionAttributes = (DrawConditionAttribute[])field.GetCustomAttributes(typeof(DrawConditionAttribute), true);
            if (drawConditionAttributes.Length > 0)
            {
                PropertyDrawCondition drawCondition = PropertyDrawConditionDatabase.GetDrawConditionForAttribute(drawConditionAttributes[0].GetType());
                return drawCondition;
            }
            else
            {
                return null;
            }
        }
    }
}
