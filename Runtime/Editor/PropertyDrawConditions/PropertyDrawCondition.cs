﻿using UnityEditor;

namespace GameSavvy.OpenUnityAttributes.Editor
{
    public abstract class PropertyDrawCondition
    {
        public abstract bool CanDrawProperty(SerializedProperty property);
    }
}
