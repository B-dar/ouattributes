using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class Sliders : MonoBehaviour
{
    [BoxGroup("Sliders")]
    [Slider(0, 10)]
    public int IntSlider;

    [BoxGroup("Sliders")]
    [Slider(0.0f, 10.0f)]
    public float FloatSlider;

    [BoxGroup("Sliders")]
    [MinMaxSlider(0.0f, 100.0f)]
    public Vector2 MinMaxSlider;
}
