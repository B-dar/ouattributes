using UnityEngine;
using GameSavvy.OpenUnityAttributes;

public class ShowHideIf : MonoBehaviour
{
    public bool Show1;
    public bool Show2;
    public bool Hide1;
    public bool Hide2;

    [ShowIf("False")]
    public int ShowIf = 0;

    [ShowIf(ConditionOperator.And, "Show1", "Show2")]
    public int ShowIfAll = 1;

    [ShowIf(ConditionOperator.Or, "Show1", "Show2")]
    public int ShowIfAny = 2;

    [HideIf("True")]
    public int HideIf = 0;

    [HideIf(ConditionOperator.And, "Hide1", "Hide2")]
    public int HideIfAll = 1;

    [HideIf(ConditionOperator.Or, "Hide1", "Hide2")]
    public int HideIfAny = 2;

    private bool True()
    {
        return true;
    }

    private bool False()
    {
        return false;
    }
}
