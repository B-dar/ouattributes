using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class ProgressBar : MonoBehaviour
{
    [ProgressBar("Health", 100, ProgressBarColor.Orange)]
    public float Health = 50;
}
