using UnityEngine;
using GameSavvy.OpenUnityAttributes;

public class ReadOnly : MonoBehaviour
{
    [ReadOnly]
    public int ReadOnlyInt = 5;
}
