using UnityEngine;
using GameSavvy.OpenUnityAttributes;

public class InfoBoxes : MonoBehaviour
{
    [InfoBox("Normal", InfoBoxType.Normal)]
    public int Int1;

    [InfoBox("Warning", InfoBoxType.Warning)]
    public int Int2;

    [InfoBox("Error", InfoBoxType.Error)]
    public int Int3;
}
