using GameSavvy.OpenUnityAttributes;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SomeStruct
{
    public int Int;
    public float Float;
    public Vector3 Vector;
}

public class ReorderableLists : MonoBehaviour
{
    [BoxGroup("Reorderable Lists")]
    [ReorderableList]
    public int[] IntArray;

    [BoxGroup("Reorderable Lists")]
    [ReorderableList]
    public List<Vector3> VectorList;

    [BoxGroup("Reorderable Lists")]
    [ReorderableList]
    public List<SomeStruct> StructList;
}
