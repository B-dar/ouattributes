using UnityEngine;
using GameSavvy.OpenUnityAttributes;
using UnityEngine.AI;

public class Label : MonoBehaviour
{
    [Label("A Short Name")]
    public string AMoreSpecificName;

    [Label("RGB")]
    public Vector3 VectorXYZ;

    [Label("Agent")]
    public NavMeshAgent NavMeshAgent;

    [Label("Ints")]
    public int[] ArrayOfInts;

    [Label("Custom Class")]
    public MyClassExample MyClass;

    [System.Serializable]
    public class MyClassExample
    {
        public int SomeInt;
        public string SomeString;
    }
}
