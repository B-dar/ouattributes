using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class NonSerializedProperties : MonoBehaviour
{

#pragma warning disable 414

    [ShowNonSerializedField]
    private int _MyInt = 10;

    [ShowNonSerializedField]
    private const float PI = 3.14159f;

    [ShowNonSerializedField]
    private static readonly Vector3 CONST_VECTOR = Vector3.one;

#pragma warning restore 414


    [ShowNativeProperty]
    public Transform ThisTransform
    {
        get
        {
            return transform;
        }
    }
}
