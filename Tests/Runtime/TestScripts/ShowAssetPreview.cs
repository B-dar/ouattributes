using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class ShowAssetPreview : MonoBehaviour
{
    [ShowAssetPreview]
    public Sprite SomeSprite;

    [ShowAssetPreview(96, 96)]
    public GameObject SomePrefab;
}
