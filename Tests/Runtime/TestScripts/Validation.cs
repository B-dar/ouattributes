using UnityEngine;
using GameSavvy.OpenUnityAttributes;

public class Validation : MonoBehaviour
{
    [MinValue(0.0f), MaxValue(1.0f)]
    public float MinMaxValidated;

    [Required]
    public Transform RequiredTransform;

    [Required("Must not be null")]
    public GameObject RequiredGameObject;

    [ValidateInput("IsNotNull", "must not be null")]
    public Sprite NotNullSprite;

    private bool IsNotNull(Sprite sprite)
    {
        return sprite != null;
    }
}
