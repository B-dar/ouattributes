using UnityEngine;
using GameSavvy.OpenUnityAttributes;
using System.Collections.Generic;

public class Dropdowns : MonoBehaviour
{
    [Dropdown("_IntValues")]
    public int IntValue;

    [Dropdown("_StringValues")]
    public string StringValue;

    [Dropdown("_VectorValues")]
    public Vector3 VectorValue;


#pragma warning disable 414

    private int[] _IntValues = new int[] { 1, 2, 3 };

    private List<string> _StringValues = new List<string>() { "A", "B", "C" };

    private DropdownList<Vector3> _VectorValues = new DropdownList<Vector3>()
    {
        { "Right", Vector3.right },
        { "Up", Vector3.up },
        { "Forward", Vector3.forward }
    };

#pragma warning restore 414

}
