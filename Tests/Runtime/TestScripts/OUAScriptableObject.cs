using GameSavvy.OpenUnityAttributes;
using UnityEngine;

// [CreateAssetMenu(fileName = "OUAScriptableObject", menuName = "OUAScriptableObject")]
public class OUAScriptableObject : ScriptableObject
{
    [SerializeField]
    [ReorderableList]
    private int[] _Ints;
}
