using UnityEngine;
using GameSavvy.OpenUnityAttributes;

public class EnableDisableIf : MonoBehaviour
{
    public bool Enable1;
    public bool Enable2;
    public bool Disable1;
    public bool Disable2;

    [EnableIf(ConditionOperator.And, "Enable1", "Enable2")]
    public int EnableIfAll = 1;

    [EnableIf(ConditionOperator.Or, "Enable1", "Enable2")]
    public int EnableIfAny = 2;

    [DisableIf(ConditionOperator.And, "Disable1", "Disable2")]
    public int DisableIfAll = 1;

    [DisableIf(ConditionOperator.Or, "Disable1", "Disable2")]
    public int DisableIfAny = 2;
}
