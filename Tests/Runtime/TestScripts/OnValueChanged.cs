using GameSavvy.OpenUnityAttributes;
using UnityEngine;

public class OnValueChanged : MonoBehaviour
{
    [OnValueChanged("OnValueChangedMethod")]
    public int OnValueChangedNumber;

    private void OnValueChangedMethod()
    {
        Debug.Log(OnValueChangedNumber);
    }
}
